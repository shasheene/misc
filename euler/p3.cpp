//sediriw 2013-02-01
#include <iostream>
#include <math.h>
using namespace std;
/*Cycles down from (target-1) through until 2, determining primes */
/*Once a prime is found, checks if factor of target*/


int main() {
  //double target=600851475143;//Actual target
  //double target=99999999;//very slow but runs
  //double target=13195;//Example target
  double target=100; //test target
  for (double i=target;i>1;i--){
    cout << "Factors " << i <<": ";
    for (double j=i-1.0; j>1;j--){
      if(fmod(i,j)==0) {
	break;
      }
      if (j==2) {
	cout <<"This is prime.";
	if ((fmod(target,i))==0) {
	  cout <<" Prime is factor of target. Quitting" << endl;
	  return (0);
	} else {
	  cout <<" Prime is not factor of target" << endl;
	}
      }
    }
  cout << endl;
  }

return 0;
}

