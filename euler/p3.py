#!/usr/bin/python3
#sediriw 2013-02-01

# First determines all possible factors pairs of target:
# from (1,target), (2,target/2), ... , (x,(target/y)<=y)
# Then removes second value of each pair (as they're not prime)
# A bunch of candidates are left over.
# Check if prime largest to smallest.

def is_prime(to_check):
    for j in range(to_check-1,1,-1):      
        if (to_check%j==0):
            return(0)
        if (j==2):
            print("  ",to_check,"is a prime AND factor. Success",)
            return(1)

#target = 13195
target=600851475143
#target = 100 #test target

search_space=int((target/2))+1 # initial search space
list = []

i=1

while i<search_space:
    print("Checking if", i,"is a factor of", target);
    candidate=i
    if (target%candidate)!=0:
        #print(" Candidate is not factor of target. Moving to next candidate",)
        i=i+1
        continue
    print(" Candidate",candidate,"is a factor of ",target,". Adding to list.")
    list.append(candidate)
    print(" ALSO SEARCH SPACE NOW REDUCED TO",target/candidate)
    search_space=(target/candidate)
    i=i+1
    continue
print("REACHED END OF SEARCH SPACE\n")

print("List contains:",list,"\nRunning over list backwards to determine primes")
for i in range(len(list)-1,0,-1):
    if(is_prime(list[i])==1):
        print("Candidate",list[i],"IS PRIME")
        exit()
    else:
        print("Candidate",list[i],"is NOT prime")  
exit()

"""



    if (is_prime(candidate)==0):
            #print(" Candidate is not a prime. ",candidate," is divisible by ",j," with no remainder")
        print("  Not prime.")
        break
    else:
 #Reached end
        print("  ",candidate,"is a prime AND factor. Success",)
        exit()
print("Nothing found",)
exit()
"""
