/*
 * By shasheene. Fri May 30 2014
 */

#include <iostream>
#include <cstdlib>
#include <cstring>
using namespace std;

void peculiarPermutive(int*, int);

int main (int argc, char** argv) {
  //  int exampleSetA[] = {2, 1, 4, 3};
  //int exampleSetB[] = {3, 8, 9, 4, 1, 7, 6, 2, 5};

  char line[256];

  cin.getline(line,256);
  int numLines = atoi(line);

  //cout << numLines << endl;

  for (int i=0;i<numLines;i++){
    cin.getline(line,256);


    int sizeInput = atoi(strtok(line," "));
    //cout << "size input" << sizeInput<< endl;
    int input[sizeInput];
    cin.getline(line,256);
    input[0] = atoi(strtok(line," "));
    //cout << input[0] << " ";
    for (int j=1;j<sizeInput;j++){
      input[j] = atoi(strtok(NULL," "));
      //cout << input[j] << " ";
    }
    //cout << endl;
    peculiarPermutive(input,sizeInput);
  }


  return (1);
}

void peculiarPermutive(int* input, int length) {
  bool doneSwaps=false;
  for (int i=0; i<length;i++){
    if (input[i]!=i+1) {
      doneSwaps=true;
      cout << "(";
      while (input[i]!=input[input[i]-1]) {
	cout << input[i] << ", ";
	//swap:
	int temp=input[i];
	input[i]=input[input[i]-1];
	input[temp-1]=temp;
      }
      cout << input[i] << ")";
    }
  }

  if (doneSwaps==false) {
    cout << "e"; //identity
  }
  cout << endl;
}
