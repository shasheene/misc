#include <iostream>
#include <time.h>
#include <cstdlib>
#include <fstream>


using namespace std;

int main (int argc, char** argv) {
  srand(time(NULL));
  int numTestCases = 50000;//rand() % 50000;

  ofstream problemFile;
  problemFile.open("problemFile");
  problemFile << numTestCases << endl;

  for (int i =0;i<numTestCases;i++){
    int permuteLength  = (rand() %50)+1;
    problemFile << permuteLength << endl;
    int numberBuffer[permuteLength];

    //generate 0..N in buffer
    for (int j =0;j< permuteLength; j++) {
      numberBuffer[j] = j+1;
    }

    //permute numbers
    for (int j=0;j<200;j++){
      int swapIndex1 = rand() % permuteLength;
      int swapIndex2 = rand() % permuteLength;
      int temp = numberBuffer[swapIndex1];
      numberBuffer[swapIndex1] = numberBuffer[swapIndex2];
      numberBuffer[swapIndex2] = temp;
    }


    //write to file
    for (int j=0;j<permuteLength;j++){
      problemFile << numberBuffer[j] << " ";
    }

    
    problemFile << endl;
  }

  problemFile.close();
  return (1);
}
