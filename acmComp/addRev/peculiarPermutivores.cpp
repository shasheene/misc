/*
 * By shasheene. Mon July 14 2014
 */

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <time.h>

#define MAXSIZE 999999

using namespace std;

void peculiarPermutive(int*, int);

int main (int argc, char** argv) {
  char line[MAXSIZE];

  cin.getline(line,MAXSIZE);
  //cout << numLines << endl;
  
  cin.getline(line,MAXSIZE);
  
  int sizeInput = atoi(strtok(line," "));
  cin.getline(line,MAXSIZE);
  
  //cout << "size input" << sizeInput<< endl;
  int input[sizeInput+1];
  input[1] = atoi(strtok(line," "));//Element 0 is dummy so indexing works
  //cout << input[1] << " ";
  for (int j=2;j<sizeInput+1;j++){
    input[j] = atoi(strtok(NULL," "));
    //cout << input[j] << " ";
  }
  //cout << endl;
  peculiarPermutive(input,sizeInput);
  
  

  return (0);
}

void peculiarPermutive(int* input, int length) {
  bool doneSwaps=false;
  for (int i=1; i<length;i++){
    if (input[i]!=i) {
      doneSwaps=true;
      cout << "(";
      while (input[i]!=input[input[i]]) {
	cout << input[i] << " ";
	//swap:
	int temp=input[i];
	input[i]=input[input[i]];
	input[temp]=temp;
      }
      cout << input[i] << ")";
    }
  }

  if (doneSwaps==false) {
    cout << "e"; //identity
  }
  cout << endl;
}
