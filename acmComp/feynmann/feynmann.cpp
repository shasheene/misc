#include <iostream>
#include <cstdlib>

int calcSquare(int a) {
  int total=0;
  for (int i=1;i<=a;i++){
    total+=i*i;
  }
  return (total);
}


int main() {

  char LINE[100];
  while(std::cin.getline(LINE,100)) {
    if (std::cin.bad()) {
      std::cerr << "Error reading from stdin D:";
    } else if  (!std::cin.eof())  {
      int inputValue;
      inputValue = atoi(LINE);
      if (inputValue==0) {
	exit (0);
      }
      std::cout << calcSquare(inputValue) << std::endl;
    } else {
      std::cerr << "Format error";
    }
  }
  return 0;
}
