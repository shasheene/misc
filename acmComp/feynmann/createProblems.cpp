#include <iostream>
#include <time.h>
#include <cstdlib>
#include <fstream>


using namespace std;

int main (int argc, char** argv) {
  srand(time(NULL));
  int numTestCases = 100;//rand() % 50000;

  ofstream problemFile;
  problemFile.open("problemFile");

  for (int i =0;i<numTestCases;i++){
    int number  = (rand() %100)+1;
    problemFile << number << endl;
  }

  problemFile.close();
  return (1);
}
