misc
=====

Miscellaneous projects probably only of interest to me.

The purpose is to document all the code I write including revision history, even relatively throw-away code like puzzle solutions so I can look back on it the same way people look back on childhood schoolwork.

(These were once seperate repositories but I integrated them so it's clearer.)